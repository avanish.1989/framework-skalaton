package utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SeleniumActions {

    private Properties properties = new Properties();
    public WebDriver driver = null;
    private final String resourceDirectory = System.getProperty("user.dir") + "\\src\\test\\resources";
    public final String orPropertiesFile = resourceDirectory + "\\propertiesFiles\\OR.properties";
    public final String configPropertiesFile = resourceDirectory + "\\propertiesFiles\\Config.properties";
    public static Logger log = Logger.getLogger("AppLogger");

    /**
     * Method Name: getPropertyValue
     * Parameters: propertyName as String
     * Return Type: String
     * Exceptions: None
     * Objective: To find the property value by using property name from the properties file
     **/

    public String getPropertyValue(String propertyFileName, String propertyName) {
        try (FileInputStream input = new FileInputStream(propertyFileName)) {
            properties.load(input);
            log.info("Property value found: " + properties.getProperty(propertyName));
            return properties.getProperty(propertyName);
        } catch (IOException exception) {
            log.fatal("Exception found while getting properties value: " + exception);
            return "Fail";
        }
    }

    /**
     * Method Name: setPropertyNameAndItsValue
     * Parameters: propertyName as String, propertyValue as String & propertiesFileName as String
     * Return Type: None
     * Exceptions: None
     * Objective: To find the web element using its locator type and locator value
     **/

    public void setPropertyNameAndItsValue(String propertyFileName, String propertyName, String propertyValue) {
        try (FileOutputStream output = new FileOutputStream(propertyFileName, false)) {
            properties.setProperty(propertyName, propertyValue);
            properties.store(output, "");
            log.info(propertyName + " has been saved to " + propertyFileName);
        } catch (IOException exception) {
            log.fatal("Exception found while writing to properties file: ", exception);
        }
    }

    /**
     * Method Name: sleep
     * Parameters: durationInSeconds as int
     * Return Type: None
     * Exceptions: None
     * Objective: To apply hard coded wait
     **/

    public void sleep(int durationInSeconds) {
        long seconds = (long) durationInSeconds;
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            log.fatal("Exception found: ", e);
        }
    }

    /**
     * Method Name: waitForJQueryToLoad
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To wait for JQuery to load
     **/

    public void waitForJQueryToLoad() {
        WebDriverWait jsWait = new WebDriverWait(driver, 10);
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        ExpectedCondition<Boolean> jQueryLoad = driver -> (Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0;
        boolean jQueryReady = (Boolean) jsExecutor.executeScript("return jQuery.active==0");

        if (!jQueryReady) {
            log.info("JQuery is not ready!!!!!!");
            jsWait.until(jQueryLoad);
        } else
            log.info("JQuery is finally ready");
    }

    /**
     * Method Name: waitForAngularToLoad
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To wait for Angular to load
     **/

    public void waitForAngularToLoad() {
        WebDriver jsWaitDriver = driver;
        WebDriverWait wait = new WebDriverWait(driver, 15);
        JavascriptExecutor jsExecutor = (JavascriptExecutor) jsWaitDriver;
        String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";
        ExpectedCondition<Boolean> angularLoad = driver -> Boolean.valueOf(((JavascriptExecutor) driver).executeScript(angularReadyScript).toString());
        boolean angularReady = Boolean.valueOf(jsExecutor.executeScript(angularReadyScript).toString());

        if (!angularReady) {
            log.info("Angular is not ready!!!!!");
            wait.until(angularLoad);
        } else {
            log.info("Angular is finally ready");
        }
    }

    /**
     * Method Name: waitUntilJSReady
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To wait for Javascript to be ready
     **/

    public void waitUntilJSReady() {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        ExpectedCondition<Boolean> jsLoad = driver -> (boolean) ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("Complete");
        boolean jsReady = (boolean) jsExecutor.executeScript("return document.readyState").toString().equals("Complete");

        if (!jsReady) {
            log.info("JS is not ready!!!!!");
            wait.until(jsLoad);
        } else {
            log.info("JS is finally ready");
        }
    }

    /**
     * Method Name: waitUntilJQueryReady
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To wait for JQuery to be ready
     **/

    public void waitUntilJQueryReady() {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        Boolean jQueryDefined = (Boolean) jsExecutor.executeScript(" return typeOf jQuery != 'undefined'");
        if (jQueryDefined) {
            sleep(15);
            waitForJQueryToLoad();
            waitUntilJSReady();
            sleep(15);
        } else {
            log.info("jQuery is not defined for the site!!!!!");
        }
    }

    /**
     * Method Name: waitUntilAngularReady
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To wait for Angular to be ready
     **/

    public void waitUntilAngularReady() {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        Boolean angularUndefined = (Boolean) jsExecutor.executeScript("return window.angular == undefined");
        if (!angularUndefined) {
            Boolean angularInjectorUndefined = (Boolean) jsExecutor.executeScript("return angular.element(document).injector() === undefined");
            if (!angularInjectorUndefined) {
                sleep(20);
                waitForAngularToLoad();
                waitUntilJSReady();
                sleep(20);
            } else {
                log.info("Angular injector is not defined on this site!!!!!");
            }
        } else {
            log.info("Angular is not defined on this site!!!!!");
        }
    }

    /**
     * Method Name: waitJQueryAngular
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To wait for Angular to be ready
     **/

    public void waitJQueryAngular() {
        waitUntilJQueryReady();
        waitUntilAngularReady();
    }

    /**
     * Method Name: openBrowser
     * Parameters: browserType as String
     * Return Type: String
     * Exceptions: None
     * Objective: To open browser instance of specific browser.
     **/

    public String openBrowser(String browserType) {
        switch (browserType.toLowerCase()) {
            case "firefox":
                FirefoxProfile ffProfile = new FirefoxProfile();
                ffProfile.setAcceptUntrustedCertificates(false);
                System.setProperty("webdriver.gecko.driver", resourceDirectory + "\\browsers\\geckodriver.exe");
                driver = new FirefoxDriver();
                log.info("Firefox launched successfully.");
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver", resourceDirectory + "\\browsers\\chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--disable-arguments");
                options.addArguments("--test-type");
                options.addArguments("-disable-notifications");
                options.addArguments("test");
                options.addArguments("disable-infobars");
                driver = new ChromeDriver(options);
                log.info("Chrome launched successfully.");
                break;
            case "headlesschrome":
                System.setProperty("webdriver.chrome.driver", resourceDirectory + "\\browsers\\chromedriver.exe");
                ChromeOptions headlessChrome = new ChromeOptions();
                headlessChrome.addArguments("--test-type");
                headlessChrome.addArguments("test");
                headlessChrome.addArguments("headless");
                driver = new ChromeDriver(headlessChrome);
                log.info("Headless Chrome launched successfully.");
            case "ie":
                System.setProperty("webdriver.ie.driver", resourceDirectory + "\\browsers\\IEDriverServer.exe");
                DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
                caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
                caps.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                driver = new InternetExplorerDriver(caps);
                log.info("Internet Explorer launched successfully.");
                break;
            default:
                log.error("Please provide a valid browser name to initiate", new IllegalArgumentException());
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        log.info("Driver initiated successfully.");
        return "Pass";
    }

    /**
     * Method Name: javascriptExecutor
     * Parameters: elementName as WebElement & action as String
     * Return Type: None
     * Exceptions: None
     * Objective: To perform an action on any element through javascript executor
     **/

    public void javascriptExecutor(WebElement elementName, String action) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        switch (action.toLowerCase()) {
            case "click":
                jsExecutor.executeScript("arguments[0].click();", elementName);
                break;
            default:
                log.info("Please enter a valid action type");
                throw new IllegalArgumentException();
        }
    }

    /**
     * Method Name: findElement
     * Parameters: locatorValueOrPropertyName as String, locatorName as String
     * Return Type: WebElement
     * Exceptions: None
     * Objective: To find the web element using its locator type and locator value
     **/

    public WebElement findElement(String locatorTypeOrProperties, String locatorValueOrPropertyName) {
        WebElement element = null;
        try {
            switch (locatorTypeOrProperties.toLowerCase()) {
                case "properties":
                    element = driver.findElement(By.xpath(getPropertyValue(locatorValueOrPropertyName, orPropertiesFile)));
                    break;
                case "id":
                    element = driver.findElement(By.id(locatorValueOrPropertyName));
                    break;
                case "name":
                    element = driver.findElement(By.name(locatorValueOrPropertyName));
                    break;
                case "tagname":
                    element = driver.findElement(By.tagName(locatorValueOrPropertyName));
                    break;
                case "classname":
                    element = driver.findElement(By.className(locatorValueOrPropertyName));
                    break;
                case "linktext":
                    element = driver.findElement(By.linkText(locatorValueOrPropertyName));
                    break;
                case "partiallink":
                    element = driver.findElement(By.partialLinkText(locatorValueOrPropertyName));
                    break;
                case "xpath":
                    element = driver.findElement(By.tagName(locatorValueOrPropertyName));
                    break;
                case "cssselector":
                    element = driver.findElement(By.cssSelector(locatorValueOrPropertyName));
                    break;
                default:
                    log.error("Please enter the valid locator type", new IllegalArgumentException());
            }
        } catch (Exception e) {
            log.fatal("Exception found while finding the element: ", e);
        }
        return element;
    }

    /**
     * Method Name: closeBrowser
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To close the browser instance
     **/

    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Method Name: findElements
     * Parameters: locatorOrPropertiesValue as String, locatorName as String
     * Return Type: List of WebElement
     * Exceptions: None
     * Objective: To find the list of web elements using its locator type and locator value
     **/

    public List<WebElement> findElements(String locatorTypeOrProperties, String locatorOrPropertiesValue) {
        List<WebElement> element = null;
        try {
            switch (locatorTypeOrProperties.toLowerCase()) {
                case "properties":
                    element = driver.findElements(By.xpath(getPropertyValue(locatorOrPropertiesValue, orPropertiesFile)));
                    break;
                case "id":
                    element = driver.findElements(By.id(locatorOrPropertiesValue));
                    break;
                case "name":
                    element = driver.findElements(By.name(locatorOrPropertiesValue));
                    break;
                case "tagname":
                    element = driver.findElements(By.tagName(locatorOrPropertiesValue));
                    break;
                case "classname":
                    element = driver.findElements(By.className(locatorOrPropertiesValue));
                    break;
                case "linktext":
                    element = driver.findElements(By.linkText(locatorOrPropertiesValue));
                    break;
                case "partiallink":
                    element = driver.findElements(By.partialLinkText(locatorOrPropertiesValue));
                    break;
                case "xpath":
                    element = driver.findElements(By.tagName(locatorOrPropertiesValue));
                    break;
                case "cssselector":
                    element = driver.findElements(By.cssSelector(locatorOrPropertiesValue));
                    break;
                default:
                    log.error("Please enter the valid locator type", new IllegalArgumentException());
            }
        } catch (Exception e) {
            log.fatal("Exception found while searching the list of elements: ", e);
        }
        return element;
    }

    /**
     * Method Name: selectOption
     * Parameters: element as WebElement, selectionType as String & optionValue as String
     * Return Type: None
     * Exceptions: None
     * Objective: To select the option from dropdown on the basis of selection type and element
     **/

    public void selectOption(WebElement element, String selectionType, String optionValue) {
        Select select = new Select(element);
        try {
            switch (selectionType.toLowerCase()) {
                case "visibletext":
                    select.selectByVisibleText(optionValue);
                    break;
                case "index":
                    int index = Integer.parseInt(optionValue);
                    select.selectByIndex(index);
                    break;
                case "value":
                    select.selectByValue(optionValue);
                    break;
                default:
                    log.error("Please enter a valid selection type", new IllegalArgumentException());
            }
        } catch (Exception exception) {
            log.fatal("Exception found while selecting the value from drop down: ", exception);
        }
    }

    /**
     * Method Name: click
     * Parameters: elementName as WebElement
     * Return Type: None
     * Exceptions: None
     * Objective: To click on the webelement
     **/

    public void click(String elementName) {
        WebElement webElement = findElement("properties", elementName);
        if (webElement.isDisplayed()) {
            waitUntilAngularReady();
            webElement.click();
        } else {
            waitUntilAngularReady();
            javascriptExecutor(webElement, "click");
        }
    }

    /**
     * Method Name: alert
     * Parameters: actionType as String
     * Return Type: None
     * Exceptions: None
     * Objective: To handle alert popup appearing on web page
     **/

    public void handleAlertPopup(String actionType) {
        Alert alert = driver.switchTo().alert();
        switch (actionType.toLowerCase()) {
            case "accept":
                alert.accept();
                break;
            case "reject":
                alert.dismiss();
                break;
            default:
                log.info("Please provide valid operation for the alert", new IllegalArgumentException());
        }
    }

    /**
     * Method Name: getTextFromElement
     * Parameters: element as String
     * Return Type: None
     * Exceptions: None
     * Objective: To get the text of the element
     **/

    public String getText(String element) {
        return findElement("properties", getPropertyValue(orPropertiesFile, element)).getText();
    }

    /**
     * Method Name: verifyExistenceOfElement
     * Parameters: element as String
     * Return Type: None
     * Exceptions: None
     * Objective: To get the text of the element
     **/

    public boolean verifyExistenceOfElement(String element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.visibilityOf(findElement("properties", element)));
            boolean status = findElement("properties", element).isDisplayed();
            if (status)
                log.info(element + "is displayed on the page");
            else
                log.info(element + "is not displayed on the page!!!!!!");
            return status;
        } catch (Exception e) {
            log.fatal("Exception found while searching the existence of the element: ", e);
            return false;
        }
    }
}

package testcases;

import org.testng.annotations.Test;
import utils.Utilities;

public class TestingFunctions extends Utilities {

    @Test
    public void launchURL() {
        driver.get(getPropertyValue("appUrl", configPropertiesFile));
        log.info("URL: " + driver.getTitle());
    }

    @Test
    public void testSetProperties() {
        setPropertyNameAndItsValue(configPropertiesFile, "googleURL", "https://www.google.com");
        setPropertyNameAndItsValue(configPropertiesFile, "title", "Google");
        setPropertyNameAndItsValue(configPropertiesFile, "browser", "chrome");
    }
    
}
